class Photo {
  String description;
  String author;
  String url;

  Photo({this.description, this.author, this.url});

  static Photo fromJson(Map<String, dynamic> map) => new Photo(
      description: map['alt_description'],
      author: map['user']['username'],
      url: map['urls']['regular']);

  @override
  String toString() {
    return 'Photo{name: $description, author: $author, url: $url}';
  }
}
