import 'package:flutter/material.dart';
import 'package:photo_list/models/photo.dart';
import 'package:photo_list/services/photo_service.dart';
import 'package:photo_list/widgets/photo_widget.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  PhotoService _photoService = PhotoService();

  int pageNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Photos"),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: pageNumber == 1
                  ? null
                  : () {
                      setState(() {
                        pageNumber--;
                        if (pageNumber < 1) {
                          pageNumber = 1;
                        }
                      });
                    },
            ),
            IconButton(
              icon: Icon(Icons.arrow_forward),
              onPressed: () {
                setState(() {
                  pageNumber++;
                });
              },
            )
          ],
        ),
        body: FutureBuilder(
          future: _photoService.getPhotos(pageNumber),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Photo> photos = snapshot.data;
              List<Widget> photoWidgets = photos.map((photo) {
                return PhotoWidget(photo);
              }).toList();

              return SingleChildScrollView(
                child: Column(children: photoWidgets),
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ));
  }
}
