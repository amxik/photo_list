import 'package:flutter/material.dart';

class PhotoPage extends StatelessWidget {
  final String url;

  PhotoPage(this.url);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image:
                DecorationImage(image: NetworkImage(url), fit: BoxFit.cover)),
      ),
    );
  }
}
