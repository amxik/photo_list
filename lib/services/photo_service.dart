import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:photo_list/models/photo.dart';

class PhotoService {
  Future<List<Photo>> getPhotos(int pageNumber) async {
    List<Photo> photos = [];

    var response = await http.get(await getPageUrl(pageNumber));

    var list = json.decode(response.body);

    for (dynamic map in list) {
      photos.add(Photo.fromJson(map));
    }

    return photos;
  }

  getPageUrl(int pageNumber) async {
    return "https://api.unsplash.com/photos?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0&page=$pageNumber";
  }
}
