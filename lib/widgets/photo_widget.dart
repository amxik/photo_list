import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:photo_list/models/photo.dart';
import 'package:photo_list/pages/photo_page.dart';

class PhotoWidget extends StatelessWidget {
  final Photo photo;

  PhotoWidget(this.photo);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 10,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PhotoPage(photo.url)));
              },
              child: Container(
                padding: EdgeInsets.all(5),
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                    image: DecorationImage(image: NetworkImage(photo.url))),
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(
                    "author : ${photo.author}",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: photo.description != null
                      ? Text(
                          photo.description,
                          softWrap: true,
                        )
                      : Text("No description"),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
